export class Patient {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    birthdate: string;
    address: string;
    contactNumber: string;
    img: string;
}