import  { Patient } from './patient';

export const PATIENTS: Patient[] = [
    {
        id: 1,
        firstName: 'Jayzon Bryan',
        middleName: 'Padua',
        lastName: 'De Leon',
        birthdate: '12/15/1994',
        address: 'Tondo, Manila',
        contactNumber: '09151430951',
        img: ''
    },
    {
        id: 2,
        firstName: 'Ken',
        middleName: 'Ny',
        lastName: 'Roggers',
        birthdate: '01/01/2001',
        address: 'Ayala, Makati',
        contactNumber: '09122344567',
        img: ''
    },
    {
        id: 3,
        firstName: 'Tony',
        middleName: 'Pepper',
        lastName: 'Stark',
        birthdate: '10/10/2010',
        address: 'BGC, Taguig',
        contactNumber: '09199876543',
        img: ''
    }
]